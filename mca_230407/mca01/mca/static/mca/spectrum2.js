var id = 0;
var taInfo01 = document.getElementById("info01");
var taInfo02 = document.getElementById("info02");
var callerFunc = "callerFunc";
var callerVal = "callerVal";
var callerResult = "callerResult";
var callerDT = "callerDT";

var idString = "GraphID: 0";

function alertFunc0(){  //Graph ID
    id = new Date(); 
    idString = "GraphID: " + id.getFullYear() + "" + (id.getMonth() + 1) + "" + id.getDate() + "" + id.getHours() + "" + id.getMinutes() + "" + id.getSeconds(); 
    console.log(idString);
    document.getElementById('graphID').innerHTML = idString;
}

alertFunc0();

var graphName = "MCA Spectrum";

var chNo = 1;
var chData = 5;
var intervalUpdate = 0;
var addupData = [];

//-------------------------------------------------------

var myInterval;

function myData(Data)
{
    console.log(Data.length);
    callerFunc = "addData";
    callerDT = "" + getDT();
    // if(Data.length > 1020){
    try
    {
        if(Data.length > 0){
            for(var x = 0; x<chNo-1; x++){
                var dInt = 0;
                try{
                    dInt = parseInt(Data[x]);
                }
                catch(err){
                    console.log("Error in string to int conversion");
                    dInt = 0;
                }
                // updateSingleData(x, dInt);
                addupSingleData(x, dInt);
                // if(x < dataLength){
                //     updateSingleData(x, dataJson[x]);
                // }
        
            }
            myChart.update();
        }
        callerVal = "" + Data.length;
        callerResult = true;
    }
    catch(err)
    {
        callerVal = "" + err;
        callerResult = false;
    }
    
    //yValue = (Data.b).toFixed(2);
    //console.log(typeof(yValue));

    console.log(callerFunc + " : " + callerVal + " : " + callerResult);

    taInfo01.value =  taInfo01.value + "\n" + callerDT + " [ " + callerFunc + " : " + callerVal + " ] ";
    console.log("scrollTop = " + taInfo01.scrollTop);
    console.log("scrollHeight = " + taInfo01.scrollHeight);
    taInfo01.scrollTop = taInfo01.scrollHeight;

    taInfo02.value =  taInfo02.value + "\n" + callerDT + " [ " + callerFunc + " : " + callerResult + " ] ";
    console.log("scrollTop = " + taInfo02.scrollTop);
    console.log("scrollHeight = " + taInfo02.scrollHeight);
    taInfo02.scrollTop = taInfo02.scrollHeight;
}

// loadJSON method to open the JSON file.
function loadJSON(path, success, error) {
var xhr = new XMLHttpRequest();
xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
    if (xhr.status === 200) {
        console.log("pico data success");
        //console.log(xhr.responseText);
        success(JSON.parse((xhr.responseText).replace(/&quot;/g, '"')));
    }
    else {
        console.log("pico data error");
        alert("Analyzer is not connected.");
        //stopGraph();
        //resetChart();
        error(xhr);
    }
    }
};
xhr.open('GET', path, true);
xhr.send();
}

function sendRequest(){
    loadJSON("http://localhost:8000/mca/spectrumData/", myData);
    //appendData(yValue);
}

function alertFunc6(){
    selectElement = document.querySelector('#port_list')
    document.getElementById("func6").innerHTML = selectElement.value;
}

function alertFunc1(){
    let text = NaN;
    callerFunc = "addChannels";
    callerDT = "" + getDT();
    do{
        text = parseInt(window.prompt("Enter a number between 1 and 2048", 5));
    }while(isNaN(text) || text > 2048 || text < 1);
    document.getElementById("func1").innerHTML = text;
    chNo = 1;
    try
    {
        removeDSLast();
        addupData = [];
        myChart.data.datasets.push(blankDataset);
        for(var x = 0; x<text; x++){
            addupData.push(0);
            addData();
            chNo++;
        }
        
        myChart.update();
        callerVal = "" + addupData.length;
        callerResult = true;
    }
    catch(err)
    {
        callerVal = "" + err;
        callerResult = false;
    }
    
    console.log(callerFunc + " : " + callerVal + " : " + callerResult);

    taInfo01.value =  taInfo01.value + "\n" + callerDT + " [ " + callerFunc + " : " + callerVal + " ] ";
    console.log("scrollTop = " + taInfo01.scrollTop);
    console.log("scrollHeight = " + taInfo01.scrollHeight);
    taInfo01.scrollTop = taInfo01.scrollHeight;

    taInfo02.value =  taInfo02.value + "\n" + callerDT + " [ " + callerFunc + " : " + callerResult + " ] ";
    console.log("scrollTop = " + taInfo02.scrollTop);
    console.log("scrollHeight = " + taInfo02.scrollHeight);
    taInfo02.scrollTop = taInfo02.scrollHeight;
}

function alertFunc2(){    //Start Button
    callerFunc = "Start";
    callerDT = "" + getDT();
    try{
        document.getElementById("func2").innerHTML = getDT();    //start
        document.getElementById("func3").innerHTML = "";    //stop
        document.getElementById("func4").innerHTML = "";    //reset
        myInterval = setInterval(sendRequest, 9000);   //note sendrequest should not contain ()

        callerVal = "" + 1;
        callerResult = true;
    }
    catch(err)
    {
        callerVal = "" + err;
        callerResult = false;
    }

    console.log(callerFunc + " : " + callerVal + " : " + callerResult);

    taInfo01.value =  taInfo01.value + "\n" + callerDT + " [ " + callerFunc + " : " + callerVal + " ] ";
    console.log("scrollTop = " + taInfo01.scrollTop);
    console.log("scrollHeight = " + taInfo01.scrollHeight);
    taInfo01.scrollTop = taInfo01.scrollHeight;

    taInfo02.value =  taInfo02.value + "\n" + callerDT + " [ " + callerFunc + " : " + callerResult + " ] ";
    console.log("scrollTop = " + taInfo02.scrollTop);
    console.log("scrollHeight = " + taInfo02.scrollHeight);
    taInfo02.scrollTop = taInfo02.scrollHeight;
}

function alertFunc3(){ //Stop Button
    callerFunc = "Stop";
    callerDT = "" + getDT();
    try{
        document.getElementById("func2").innerHTML = "";    //start
        document.getElementById("func3").innerHTML = getDT();    //stop
        document.getElementById("func4").innerHTML = "";    //reset
        clearInterval(myInterval);

        callerVal = "" + 1;
        callerResult = true;
    }
    catch(err)
    {
        callerVal = "" + err;
        callerResult = false;
    }

    console.log(callerFunc + " : " + callerVal + " : " + callerResult);

    taInfo01.value =  taInfo01.value + "\n" + callerDT + " [ " + callerFunc + " : " + callerVal + " ] ";
    console.log("scrollTop = " + taInfo01.scrollTop);
    console.log("scrollHeight = " + taInfo01.scrollHeight);
    taInfo01.scrollTop = taInfo01.scrollHeight;

    taInfo02.value =  taInfo02.value + "\n" + callerDT + " [ " + callerFunc + " : " + callerResult + " ] ";
    console.log("scrollTop = " + taInfo02.scrollTop);
    console.log("scrollHeight = " + taInfo02.scrollHeight);
    taInfo02.scrollTop = taInfo02.scrollHeight;
}

function alertFunc4(){ //Reset Button
    callerFunc = "Reset";
    callerDT = "" + getDT();
    try{
        document.getElementById("func2").innerHTML = "";    //start
        document.getElementById("func3").innerHTML = "";    //stop
        document.getElementById("func4").innerHTML = getDT();    //reset
        for(var x = 0; x<addupData.length; x++){
            addupData[x] = 0;
            updateSingleData(x, 0);
        }
        myChart.update();

        callerVal = "" + 1;
        callerResult = true;
    }
    catch(err)
    {
        callerVal = "" + err;
        callerResult = false;
    }

    console.log(callerFunc + " : " + callerVal + " : " + callerResult);

    taInfo01.value =  taInfo01.value + "\n" + callerDT + " [ " + callerFunc + " : " + callerVal + " ] ";
    console.log("scrollTop = " + taInfo01.scrollTop);
    console.log("scrollHeight = " + taInfo01.scrollHeight);
    taInfo01.scrollTop = taInfo01.scrollHeight;

    taInfo02.value =  taInfo02.value + "\n" + callerDT + " [ " + callerFunc + " : " + callerResult + " ] ";
    console.log("scrollTop = " + taInfo02.scrollTop);
    console.log("scrollHeight = " + taInfo02.scrollHeight);
    taInfo02.scrollTop = taInfo02.scrollHeight;
}
//---------------------------------------------------------


const blankDataset = {
    label: 'Spectrum',
    data: [],
    backgroundColor: [],
    borderColor: [],
    borderWidth: 1
    //fill: 'origin',
    //showLine: false,
    //pointRadius: 1
};

const blankLabel = [];

const data = {
    labels: [],
    datasets: [blankDataset]
};

const bgColor = {
    id: 'bgColor',
    beforeDraw: (chart, steps, options) => {
      const {ctx, width, height } = chart;
      ctx.fillStyle = options.backgroundColor;
      ctx.fillRect(0,0,width, height);
      ctx.restore();
    }
}

// config 
const config = {
    type: 'bar',
    data,
    options: {
        scales: {
            y: {
            beginAtZero: true
            }
        },
        animation: true,
        plugins:{
            bgColor:{
              backgroundColor: 'white'
            }
        }
    },
    plugins: [bgColor]
};

// const canvas = new HTMLCanvasElement();
// const offscreenCanvas = canvas.transferControlToOffscreen();
// const worker = new worker('worker.js');
// worker.postMessage({canvas: offscreenCanvas, config}, [offscreenCanvas]);

// render init block
const myChart = new Chart(
    document.getElementById('myChart'),
    config
);

function removeDSLast(){
    console.log("Remove Dataset Last");
    myChart.data.datasets.pop();
    myChart.data.labels = [];
    myChart.update();
}



function addData(){
    // console.log("Add data");
    //info01.value = "\n Added chNo: " + chNo;
    myChart.data.labels.push(""+chNo);
    myChart.data.datasets[0].data.push(chData);
    myChart.data.datasets[0].backgroundColor.push('rgba(0, 0, 0, 0.2)');
    myChart.data.datasets[0].borderColor.push('rgba(0, 0, 0, 1)');
    
    //myChart.update();
    
}

function updateSingleData(chNo, chData){
    // console.log("Update Single data");
    //info01.value = "\n Update chNo: " + (chNo) + " Data: " + chData;
    var colData = chData;
    while(colData > 255){
        colData = colData/2;
    }
    console.log(colData);
    var chColor = 'rgba('+ Math.floor(colData) +', '+ Math.floor(colData/2) +', '+ Math.floor(colData/2) +', '+ '0.6' + ')';
    myChart.data.datasets[0].data[chNo] = chData;
    myChart.data.datasets[0].backgroundColor[chNo] = chColor;
    myChart.data.datasets[0].borderColor[chNo] = chColor;
    // myChart.update();
}

function addupSingleData(chNo, chData){
    // console.log("Update Single data");
    //info01.value = "\n Update chNo: " + (chNo) + " Data: " + chData;   
    
    
    //addupData[chNo] = addupData[chNo] + chData + Math.floor(Math.random()*1000); //remove this random number for actual use
    addupData[chNo] = addupData[chNo] + chData; 
    var colData = addupData[chNo];
    while(colData > 255){
        colData = parseInt(colData/2);
    }
    console.log("colData: " + colData);
    var chColor = 'rgba('+ Math.floor(colData) +', '+ Math.floor(colData/2) +', '+ Math.floor(colData/2) +', '+ '0.6' + ')';
    myChart.data.datasets[0].data[chNo] = addupData[chNo];   
    myChart.data.datasets[0].backgroundColor[chNo] = chColor;
    myChart.data.datasets[0].borderColor[chNo] = chColor;
    // myChart.update();
}

function addZero(data){
	if(data < 10){
		data = '0' + data;
	}
	return data;
}
function getDT(){
	var today = new Date();
	var yr = addZero(today.getFullYear() - 2000);
	var mon = addZero(today.getMonth() + 1);
	dateString = addZero(today.getFullYear() - 2000) + '/' + addZero(today.getMonth() + 1) + '/' + addZero(today.getDate()) + ' ' + addZero(today.getHours())+ ':' + addZero(today.getMinutes())+ ':' + addZero(today.getSeconds());
	console.log("dateString: " + dateString);
	return dateString;
}

// function alertFunc1(){
//     let text = NaN;
//     do{
//         text = parseInt(window.prompt("Enter a number between 1 and 100", 5));
//     }while(isNaN(text) || text > 2048 || text < 1);
//     document.getElementById("func1").innerHTML = text;
//     chNo = 1;
//     removeDSLast();
//     myChart.data.datasets.push(blankDataset);
//     for(var x = 0; x<text; x++){
//         addupData.push(0);
//         addData();
//         chNo++;
//     }
//     console.log("addupData length: " + addupData.length);
//     myChart.update();
// }





function testUpdateSingleData(){
    var dataString = document.getElementById("specData").textContent;
    console.log(dataString);
    var dataJson = JSON.parse(dataString);
    console.log(typeof(dataJson));
    console.log(dataJson[0]);
    var dataLength = dataJson.length;
    console.log(dataLength);
    for(var x = 0; x<chNo; x++){
        updateSingleData(x, Math.floor(Math.random()*1000));
        // if(x < dataLength){
        //     updateSingleData(x, dataJson[x]);
        // }

    }
    myChart.update();
}

// setInterval(test02, 5000);

// function test02(){
//     updateSingleData(intervalUpdate, Math.floor(Math.random()*1000));
//     console.log(chNo);
//     if(intervalUpdate < chNo){
//         intervalUpdate++;
//     }
//     else{
//         intervalUpdate = 0;
//     }
// }

function alertFunc5(){       //Report Button
    callerFunc = "Report";
    callerDT = "" + getDT();
    try{
        const canvas = document.getElementById('myChart');
        const canvasImage = canvas.toDataURL('image/jpeg', 1.0);
        console.log(canvasImage);
    
        var topGap = 15;
    
        let pdf = new jsPDF();
        let dt = id.getFullYear() + "-" + (id.getMonth() + 1) + "-" + id.getDate();
        
        let dataPoints = myChart.data.datasets[0].data.length;
        let startData = 0;
        let endData = 0;
        if(dataPoints > 0){
        startData = myChart.data.datasets[0].data[0];
        endData = myChart.data.datasets[0].data[dataPoints - 1];
        }
    
        pdf.setFontSize(14);
        pdf.text(60, topGap, "MCA Spectrum " + idString);
        topGap = topGap + 5;
        // pdf.text(10, topGap, "------------------------------------------------------------------------------------------------------------------");
        // topGap = topGap + 6;
        // pdf.text(20, topGap, idString);
        // pdf.text(130, topGap, "Report No: " + reportNo);
        // topGap = topGap + 5;
        // pdf.text(20, topGap, "Analyser: " + analyserModel);
        // pdf.text(130, topGap, "Date: " + dt);
        // topGap = topGap + 5;
        pdf.text(10, topGap, "------------------------------------------------------------------------------------------------------------------");
        topGap = topGap + 10;
        pdf.addImage(canvasImage, 'JPEG', 15, topGap, 180, 100);
        topGap = topGap + 120;
        pdf.text(10, topGap, "------------------------------------------------------------------------------------------------------------------");
        // topGap = topGap + 6;
        // dt = startDT.getFullYear() + "-" + (startDT.getMonth() + 1) + "-" + startDT.getDate() + "T" + startDT.getHours() + ":" + startDT.getMinutes() + ":" + startDT.getSeconds(); 
        // pdf.text(20, topGap, "Start:     " + dt + "            Initial Conc.    " + startData + " (" + gasName + ")");
        // topGap = topGap + 5;
        // dt = endDT.getFullYear() + "-" + (endDT.getMonth() + 1) + "-" + endDT.getDate() + "T" + endDT.getHours() + ":" + endDT.getMinutes() + ":" + endDT.getSeconds();
        // pdf.text(20, topGap, "End:      " + dt + "              Final Conc.     " + endData + " (" + gasName + ")");
        // topGap = topGap + 5;
        // pdf.text(10, topGap, "------------------------------------------------------------------------------------------------------------------");
        // topGap = topGap + 60;
        // pdf.text(15, topGap, "Tested/Authorised By");
        // pdf.text(150, topGap, "Witnessed By");
    
    
    
        topGap = topGap + 40;
        pdf.setFontSize(8);
        var footTxt = "--------------------------- Sairish Scientific & IndianRobots (Make in INDIA) ---------------------------";
        pdf.text(50, topGap, footTxt);
    
        pdf.save(graphName + "_" + getDT() + '.pdf');

        callerVal = "" + 1;
        callerResult = true;
    }
    catch(err)
    {
        callerVal = "" + err;
        callerResult = false;
    }

    console.log(callerFunc + " : " + callerVal + " : " + callerResult);

    taInfo01.value =  taInfo01.value + "\n" + callerDT + " [ " + callerFunc + " : " + callerVal + " ] ";
    console.log("scrollTop = " + taInfo01.scrollTop);
    console.log("scrollHeight = " + taInfo01.scrollHeight);
    taInfo01.scrollTop = taInfo01.scrollHeight;

    taInfo02.value =  taInfo02.value + "\n" + callerDT + " [ " + callerFunc + " : " + callerResult + " ] ";
    console.log("scrollTop = " + taInfo02.scrollTop);
    console.log("scrollHeight = " + taInfo02.scrollHeight);
    taInfo02.scrollTop = taInfo02.scrollHeight;
}
  
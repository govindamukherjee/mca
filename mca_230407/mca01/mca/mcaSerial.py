import serial
from time import sleep
from serial.tools.list_ports import comports
import re

class SerialMCA:

    def __init__(self, portID):
        self.portID = portID
        self.mcaSerial = None
    
    def __str__(self):
        return "Connect by passing USB to Serial ID part and get Data"

    def getPortList(self):
        self.portList = []
        self.mcaPort = ""
        for port in comports():
            self.portList.append(port)
            print(f'Current port: {port}')
        for port in self.portList:
            x = re.search(self.portID, str(port))
            if(x):
                self.mcaPort = str(port)[0:4]
        print(f'mca port: {self.mcaPort}')

    #if port is open then return true
    #if port is busy or closed return false
    def checkPortStatus(self):
        if(self.mcaSerial == None):
            return False
        else:
            try:
                if(self.mcaSerial.is_open):
                    return True
                else:
                    return False
            except:
                return False
                

    def openMCA(self):
        if(not self.checkPortStatus()):
            if(self.mcaPort != ""):
                try:
                    self.mcaSerial = serial.Serial(self.mcaPort)
                    self.mcaSerial.baudrate = 19200
                    print(self.mcaSerial)
                except:
                    print("Error in opening Port")
                sleep(1)

    def closeMCA(self):
        if(self.checkPortStatus()):
            try:
                self.mcaSerial.close()
                print(self.mcaSerial)
            except:
                print("Error in closing Port")
            sleep(1)

    mcaData = []
    def readMCA(self):
        #self.mcaData = []
        if(self.checkPortStatus()):
            if(self.mcaSerial.in_waiting > 0):
                temp = self.mcaSerial.readline()
                try:
                    self.mcaData = temp.decode("Ascii")[1:-4].split()
                    #print(self.mcaData)
                except:
                    self.mcaData = []
        return self.mcaData

    def testMCA(self):
        self.getPortList()
        self.openMCA()
        for x in range(20):
            self.readMCA()
            sleep(1)
        self.closeMCA()

def main():
    mca01 = SerialMCA("CP210x")
    mca01.testMCA()  
    print("main")

if __name__ == "__main__":
    main()
    

        
    
    

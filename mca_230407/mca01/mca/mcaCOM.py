import serial
from time import sleep
from serial.tools.list_ports import comports
import re

class COMmca:

    def __init__(self):
        self.mcaSerial = None
    
    def __str__(self):
        return "Connect by passing USB to Serial and select COM"

    def getPortList(self):
        self.portList = []
        self.mcaPort = ""
        for port in comports():
            self.portList.append(str(port)[0:4])
            print(f'Current port: {port}')
        return self.portList

    #if port is open then return true
    #if port is busy or closed return false
    def checkPortStatus(self):
        if(self.mcaSerial == None):
            return False
        else:
            try:
                if(self.mcaSerial.is_open):
                    return True
                else:
                    return False
            except:
                return False
                

    def openMCA(self, comPort = "COM0"):
        openStatus = False
        if(not self.checkPortStatus()):
            if(comPort != ""):
                try:
                    self.mcaSerial = serial.Serial(comPort)
                    self.mcaSerial.baudrate = 19200
                    openStatus = True
                    print(self.mcaSerial)
                except:
                    openStatus = False
                    print("Error in opening Port")
                sleep(1)
        return openStatus

    def closeMCA(self, comPort = "COM0"):
        closeStatus = False
        if(self.checkPortStatus()):
            try:
                self.mcaSerial.close()
                closeStatus = True
                print(self.mcaSerial)
            except:
                closeStatus = False
                print("Error in closing Port")
            sleep(1)
        return closeStatus

    mcaData = []
    def readMCA(self):
        #self.mcaData = []
        if(self.checkPortStatus()):
            if(self.mcaSerial.in_waiting > 0):
                temp = self.mcaSerial.readline()
                try:
                    self.mcaData = temp.decode("Ascii")[1:-4].split()
                    #print(self.mcaData)
                except:
                    self.mcaData = []
        return self.mcaData

    def testMCA(self):
        self.getPortList()
        print(self.portList)
        openStatus = self.openMCA()
        print(openStatus)
        openStatus = self.openMCA("COM3")
        print(openStatus)
        closeStatus = self.closeMCA()
        print(closeStatus)
        closeStatus = self.closeMCA("COM3")
        print(closeStatus)

def main():
    mca01 = COMmca()
    mca01.testMCA()  
    print("main")

if __name__ == "__main__":
    main()
    

        
    
    

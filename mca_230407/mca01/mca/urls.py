from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('spectrum/', views.spectrum, name='spectrum'),
    path('spectrumData/', views.spectrumData, name='spectrumData'),
    path('htmlData/', views.htmlData, name='htmlData'),
]

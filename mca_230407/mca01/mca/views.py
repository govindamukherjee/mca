from django.shortcuts import render
from django.http import HttpResponse
from json import dumps
import subprocess
import random
from mca.mcaSerial import SerialMCA
from mca.mcaCOM import COMmca
from time import sleep
import threading
from django.http import JsonResponse

# mca01 = SerialMCA("CP210x")
# mca01.getPortList()
# mca01.closeMCA()
# mca01.openMCA()
mca01 = COMmca()
mca01.getPortList()
print(mca01.portList)
usbPortMCA = "COM0"

threadData = []

def callMCAThread():
    global threadData
    threading.Timer(4.0, callMCAThread).start()
    print("Govinda")
    if(mca01.checkPortStatus()):
        x = mca01.readMCA()
        print("x: ", len(x))
        if(len(x) > 1020):
            threadData = x
            print("threadData: ", len(threadData))
    else:
        print("mca port not connected")
    

callMCAThread()       

def index(request):
    return HttpResponse("Hello")

def spectrum(request):
    global mca01
    data = [1, 2, 3, 4, random.randint(1, 101)]
    jsonData = dumps(data)
    context = {'ipString': jsonData, 'usbPorts': mca01.portList}
    return render(request, 'mca/spectrum.html', context)

def spectrumData(request):
    jsonData = dumps(threadData)
    context = {'spectrumData': jsonData, }
    return render(request, 'mca/spectrumData.html', context)

def htmlData(request):
    global usbPortMCA, mca01
    myCall = "myCall"
    myValue = "myValue"
    myResult = "myResult"
    print("Data received from html")
    if request.method == 'POST':
        myCall = request.POST.get('calledFrom')
        myValue = request.POST.get('htmlToViewsValue')
        print(myCall + " : " + myValue)
    if(myCall=="connect"):
        print("closeStatus: ", mca01.closeMCA(myValue))
        sleep(0.1)
        myResult = mca01.openMCA(myValue)
        sleep(0.1)
    data = {"Call": myCall, "Value": myValue, "Result": myResult}
    return JsonResponse(data, safe=False)